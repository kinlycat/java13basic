import java.util.Scanner;

/*Напишите программу, которая вычисляет стоимость проезда в такси
в зависимости от расстояния из расчета 8 руб. 50 коп. за километр.
Ваша программа должна взаимодействовать с пользователем следующим образом:

СЧЕТЧИК СТОИМОСТИ ПРОЕЗДА
Введите начальные показания одометра: 13505
Введите конечные показания одометра: 13810
Вы проехали 305 км. Из расчета 8 руб. 50 коп. за км,
стоимость проезда равна 2592 руб. 50 коп.
 */
public class priceTaxi {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        //final double pricePerKm = 8.5;
        int beginOdometr, endOdometr, myPath, rub;
        double pricePerKm, finalPrice, kop;

        pricePerKm = 8.5;

        System.out.println("СЧЕТЧИК СТОИМОСТИ ПРОЕЗДА");
        System.out.print("Введите начальные показания одометра: ");
        beginOdometr = input.nextInt();

        System.out.print("Введите конечные показания одометра: ");
        endOdometr = input.nextInt();

        myPath = endOdometr - beginOdometr;
        finalPrice = myPath * pricePerKm;
        //(finalPrice*100)/100.0
        rub = (int)finalPrice;
        kop = finalPrice % 100;

        System.out.println("Вы проехали " + myPath + "км");
        System.out.println("Из расчета 8 руб. 50 коп. за км. Cтоимость проезда равна " + (int)finalPrice +
                " руб. " + (int)finalPrice*100 % 100 + " коп");





    }
}
