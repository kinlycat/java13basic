import java.util.Scanner; // Scanner находится в пакете java.util

//вычисление среднее геометрическое 3 чисел
public class Average {
    public static void main(String[] args) {
        double number1, number2, number3, average; // объявление переменных

        Scanner input = new Scanner(System.in); // создание объекта типа Scanner

        // Получить три числа
        System.out.print("Введите три вещественных числа, разделенные пробелами: ");
        number1 = input.nextDouble();
        number2 = input.nextDouble();
        number3 = input.nextDouble();

        // Вычислить среднее арифметическое трех чисел
        average = Math.cbrt(number1 * number2 * number3);


        // Отобразить среднее арифметическое трех чисел
        System.out.println("Среднее геометрическое " + number1 + " " + number2
                + " " + number3 + " равно " + average + ".");
    }
}