import java.util.Scanner;

public class Celsius {
    public static void main(String[] args) {
        double fahrenheit, celsius;

        Scanner input = new Scanner(System.in);

        // Получить температуру в градусах по шкале цельсия
        System.out.print("Введите температуру в градусах по шкале цельсия: ");
        celsius = input.nextDouble();

        // Преобразовать температуру в градусы по шкале Цельсия
        fahrenheit = (9.0 / 5) * celsius + 32.0;

        // Отобразить температуру в градусах по шкале Фаренгейт
        System.out.println(celsius + " градуса по шкале Цельсия равно " + fahrenheit + " градуса по шкале Фаренгейта ");

    }
}