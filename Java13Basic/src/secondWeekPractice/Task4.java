package secondWeekPractice;
import java.util.Scanner;

/*
Считать данные из консоли о типе номера отеля
1 - VIP, 2 - Premium, 3 - Standart
Вывести цену номера VIP = 125, Premium = 110, Standart = 100
 */
public class Task4 {
    public static final int VIP_PRICE = 125;
    public  static final int PREMIUM = 110;
    public static  final int STANDART = 100;

    public static void main(String[]args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите тип номера: 1 - VIP, 2 - Premium, 3 - Standart");
        int roomType = scanner.nextInt();
        // Вариант 1
        /*if (roomType == 1) {
            System.out.println("VIP price: " + VIP_PRICE);
        } else if (roomType == 2) {
            System.out.println("Premium price: " + PREMIUM);
        } else if (roomType == 3 {
            System.out.println("Standart price: " + STANDART);
        }
        else System.out.println("Введите корректный номер!");
        */
        //Вариант 2
        /*
               switch (roomType){
            case 1:
                System.out.println("VIP price: " + VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium price: " + PREMIUM);
                break;
            case 3:
                System.out.println("Standart price: " + STANDART);
                break;
            default: System.out.println("Введите корректный номер!");

        }*/
        //Вариант 3 через лямбды
        switch (roomType) {
            case 1 -> System.out.println("VIP price: " + VIP_PRICE);
            case 2 -> System.out.println("Premium price: " + PREMIUM);
            case 3 -> System.out.println("Standart price: " + STANDART);
            default -> System.out.println("Введите корректный номер!");
        }

    }
}

